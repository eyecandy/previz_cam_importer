bl_info = {
    "name": "Previs Camera Importer",
    "author": "Andy Goralczyk",
    "version": (0, 1),
    "blender": (2, 80, 0),
    "location": "Import menu",
    "description": "",
    "warning": "",
    "doc_url": "",
    "category": "Import-Export",
}

import bpy
from bpy_extras.io_utils import ImportHelper 
from bpy.types import Operator

from bpy.props import BoolProperty

shot_marker = '+'
scene_start_frame = 101
    

class PCI_import_cameras(Operator, ImportHelper):
    """ Import cameras from a previs file. The collection needs to follow a certain naming scheme. Example: sc06.0050.v001 [120-160] +"""
    bl_idname = "previs_cam_import.import_cameras"
    bl_label = "Import Cameras"

    clear_scene_markers: BoolProperty(name="Clear scene timeline markers", default=True) 
    set_scene_frame_range: BoolProperty(name="Set scene frame range", default=True)
    append_existing: BoolProperty(name="Append to existing shots", default=False)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        active_collection = bpy.context.collection
        scene = bpy.context.scene
        current_marker = scene_start_frame

        if self.append_existing:
            current_marker =+ scene.frame_end

        bpy.ops.object.select_all(action='DESELECT')

        with bpy.data.libraries.load(self.filepath, link=True) as (data_from, data_to):
            data_to.collections = [name for name in data_from.collections if name.endswith(shot_marker)]

        for col in data_to.collections:
            col_empty_copy = bpy.data.collections.new(col.name)
            active_collection.children.link(col_empty_copy)

            for col_child in col.children:
                if col_child.name.startswith("CA"):
                    for ob in col_child.objects:
                        col_empty_copy.objects.link(ob)
        
        if self.set_scene_frame_range:
            if not self.append_existing:
                scene.frame_start = scene_start_frame

        if self.clear_scene_markers:
            scene.timeline_markers.clear()

        # Iterate through shot collections
        for col in bpy.data.collections: 
            if col.name.endswith(shot_marker):
                #print(f"currently looking at collection {col.name}")
                shot_details = get_shot_details(col)
                shot_length = shot_details['frame_end'] - shot_details['frame_start']

                if 'shot_camera' not in shot_details or 'shot_camrig' not in shot_details:
                    continue

                shot_camera = shot_details['shot_camera']
                shot_camera = shot_camera.make_local(clear_liboverride=True)
                shot_camera.data.make_local(clear_liboverride=True)
                #print(f"shot details are {shot_details}")
                shot_camrig = shot_details['shot_camrig']
                shot_camrig = shot_camrig.make_local(clear_liboverride=True)
                shot_camrig.data.make_local(clear_liboverride=True)

                view_layer = bpy.context.view_layer
                view_layer.objects.active = shot_camera

                shot_camera.select_set(True)

                shot_camera.name = f"CAM-{shot_details['shotname']}"
                shot_camrig.name = f"RIG-camera-{shot_details['shotname']}"

                bpy.ops.nla.bake(frame_start=shot_details['frame_start'], frame_end=shot_details['frame_end'], visual_keying=True, clear_constraints=True, clear_parents=True, use_current_action=True, clean_curves=True, bake_types={'OBJECT'})

                camera_offset = -(shot_details['frame_start']) + current_marker
                move_keys(shot_camera, camera_offset)
                move_keys(shot_camrig, camera_offset)

                marker = scene.timeline_markers.new(shot_details['shotname'], frame = current_marker)
                marker.camera = shot_camera 
                current_marker = current_marker + shot_length

                col.name = shot_details['shotname']

        if self.set_scene_frame_range:
            scene.frame_end = current_marker
            
        bpy.ops.outliner.orphans_purge(do_local_ids=True, do_linked_ids=True, do_recursive=True)
        return {'FINISHED'}
    

def get_shot_details(col):
    '''
    Splits the collection name into its components. We assume this type of pattern: sc06.0050.v001 [120-160] +
    '''
    colname = col.name

    shot_details = dict()
    colname = colname.replace("[", "$")
    colname = colname.replace("]", "$")
    colname_split = colname.split("$")
    shot_details['shotname'] = colname_split[0].strip()
    frame_range = colname_split[1]

    shot_details['frame_start'] = int(frame_range.split("-")[0])
    shot_details['frame_end'] = int(frame_range.split("-")[1])

    for ob in col.objects:
        if ob.name.startswith("CAM"): 
            shot_details['shot_camera'] = ob
        if ob.name.startswith("RIG"):
            shot_details['shot_camrig'] = ob
    return shot_details

def move_keys(ob, offset):
    anim_data = ob.animation_data
    if not anim_data:
        return

    action = anim_data.action
    if not action:
        return

    for fc in action.fcurves:
        for kp in fc.keyframe_points:
            kp.co[0]            += offset
            kp.handle_left[0]   += offset
            kp.handle_right[0]  += offset

def draw_menu(self, context):
    layout = self.layout
    layout.operator(PCI_import_cameras.bl_idname, text = 'Cameras from previs', icon = 'VIEW_CAMERA')

cls = (
    PCI_import_cameras,
)

def register():
    for cl in cls:
        bpy.utils.register_class(cl)
    bpy.types.TOPBAR_MT_file_import.append(draw_menu)


def unregister():
    for cl in cls:
        bpy.utils.unregister_class(cl)
    bpy.types.TOPBAR_MT_file_import.remove(draw_menu)